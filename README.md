# Projet Cathédrale

## Fonctionnalités Implémentées

* Déplacement
* Déplacement limité au sein de la structure du Transept
* Ajout d'un brouillard
* Conception de la fonction de création de Pancarte

## Touches de Déplacement

* Pour le déplacement vertical vous pouvez utilisez ArrowUp, ArrowDown, Z et S.
* Vous pouvez orientez votre caméra de Droite à Gauche à l'aide des touches ArrowLeft, ArrowDown, Q et D.
* Vous pouvez vous déplacement horizontalement à l'aide des touches SHIFT+ArrowLeft, SHIFT+ArrowDown, SHIFT+Q et SHIFT+D.
* Vous pouvez contempler le plafond ou le sol à l'aide de SHIFT+ArrowUp, SHIFT+ArrowDown, SHIFT+Z et SHIFT+S. Cependant lors de cette translation les autres déplacement sont bloqués jusqu'au retour à la position initiale.
* Vous pouvez regarder à nouveau face à vous à l'aide de la touche Insertion.
* Vous pouvez retourner à la position initiale à l'aide de la touche Echap.

## Auteurs

Féry Mathieu DUT2G5 (Aka Mathius)
Paul Rosselle DUT2G5
