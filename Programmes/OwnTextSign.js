const labelGeometry = new THREE.PlaneBufferGeometry(1, 1);

function makeLabelCanvas(size, text) {
    const borderSize = 2;
    const ctx = document.createElement('canvas').getContext('2d');
    const font =  `${size}px bold sans-serif`;
    ctx.font = font;

    const doubleBorderSize = borderSize * 2;
    const width = ctx.measureText(text).width + doubleBorderSize;
    const height = size + doubleBorderSize;
    ctx.canvas.width = width;
    ctx.canvas.height = height;

    ctx.font = font;
    ctx.textBaseline = 'top';

    ctx.fillStyle = 'black';
    ctx.fillRect(0, 0, width, height);
    ctx.fillStyle = 'white';
    ctx.fillText(text, borderSize, borderSize);

    return ctx.canvas;
}

function makeSign(pos_y, pos_x, pos_z, size, text) {
    const canvas = makeLabelCanvas(size, text);
    const texture = new THREE.CanvasTexture(canvas);
    texture.minFilter = THREE.LinearFilter;
    texture.wrapS = THREE.ClampToEdgeWrapping;
    texture.wrapT = THREE.ClampToEdgeWrapping;

    const labelMaterial = new THREE.MeshBasicMaterial({
    map: texture,
    side: THREE.DoubleSide,
    transparent: true,
    });

    const label = new THREE.Mesh(labelGeometry, labelMaterial);
    scene.add(label);
    label.position.x = pos_x;
    label.position.y = pos_y;
    label.position.z = pos_z;
}