function Cath() {
    var container, camera, scene, renderer;
    var clavier = {};
    var speed = 0.05;
    var look = Math.PI / 128;
    var lock = false;
    var  up = 0;
    var down = 0;
    function onWindowResize() {
    camera.aspect = window.innerWidth / window.innerHeight;
        camera.updateProjectionMatrix();
        renderer.setSize( window.innerWidth, window.innerHeight );
    }
    function Rot_Init()
    {
        up = 0;
        down = 0;
        lock = false;
    }
    function Position_Initiale(camera)
    {
        camera.rotation.x=Math.PI/2;
        camera.rotation.y=0;
        camera.rotation.z=0;
        camera.position.x=0;
        camera.position.y=0;
        camera.position.z=-20;
        Rot_Init();
        return camera;
    }
    function Init() {
        container = document.createElement( 'div' );
        document.body.appendChild( container );

        camera = new THREE.PerspectiveCamera( 90, window.innerWidth / window.innerHeight, 1, 2000 );
        camera = Position_Initiale(camera);

        // scene
        scene = new THREE.Scene();

        var ambient = new THREE.AmbientLight( 0xffffff );
        scene.add( ambient );

        // texture

        var manager = new THREE.LoadingManager();
        manager.onProgress = function ( item, loaded, total ) {
            console.log( item, loaded, total );
        };

        var texture = new THREE.Texture();

        var loader = new THREE.ImageLoader( manager );
        loader.load( 'TranseptSud/TranseptTexture4096.jpg', function ( image ) {
            texture.image = image;
            texture.needsUpdate = true;
        } );

        // Chargement du modèle
        loader = new THREE.OBJLoader( manager );
        loader.load( 'TranseptSud/transeptSudBox.obj', function ( object ) {
            object.traverse( function ( child ) {
                if ( child instanceof THREE.Mesh ) {
                    child.material.map = texture;
                }
            } );
            scene.add( object );
        } );
        renderer = new THREE.WebGLRenderer();
        renderer.setSize( window.innerWidth, window.innerHeight );
        container.appendChild( renderer.domElement );
        window.addEventListener( 'resize', onWindowResize, false );

        // Gestion des déplacements
        controls = new THREE.PointerLockControls(camera);
        controls.enabled = false;
        scene.add(controls.getObject());
        makeSign(0, 0, -20, 80, "Blap", scene);
    }
    function Afficher() {
        renderer.render(scene,camera);
    }
    function Animer() {
        requestAnimationFrame(Animer);
        Move();
        Frontiere();
        Afficher();
    }
    function InitFog()
    {
        scene.fog = new THREE.Fog(0x000000, 0.8, 4.2); // color | near | far
    }
    function Cancel_Rot()
    {
        if (up != 0)
            controls.getObject().rotateX(-look * up);
        else
            controls.getObject().rotateX(look * down);
        Rot_Init();
    }
    function ViewUp(controls, other, here, direction=1)
    {
        if (other != 0) {
            Cancel_Rot();
        }
        else {
            lock = true;
            controls.getObject().rotateX(direction * look);
            here += 1;
        }
        return here;
    }
    function Move()
    {
        if (clavier[45])
            Cancel_Rot();
        if (clavier[27])
            camera = Position_Initiale(camera);
        if (clavier[90] || clavier[38]) {
            if (!clavier[16]) {
                if (!lock)
                    controls.getObject().translateZ(-speed);
            }
            else {
                up = ViewUp(controls, down, up);
                down = 0;
            }
        }
        if (clavier[83] || clavier[40]) {
            if (!clavier[16]) {
                if (!lock)
                    controls.getObject().translateZ(speed);
            }
            else {
                down = ViewUp(controls, up, down, -1);
                up = 0;
            }
        }
        if (!lock) {
            if (clavier[81] || clavier[37]){
                if (clavier[16])
                    controls.getObject().translateX(-speed);
                else
                    controls.getObject().rotateY(look);
            }
            if (clavier[68] || clavier[39]){
                if (clavier[16])
                    controls.getObject().translateX(speed);
                else
                    controls.getObject().rotateY(-look);
            }
        }	
    }
    function Frontiere()
    {
        if(camera.position.x>=3.95)
        {
            camera.position.x=3.95;
        }
        if(camera.position.x<=-3.95)
        {
            camera.position.x=-3.95;
        }
        if(camera.position.y>=10.40)
        {
            camera.position.y=10.40;
        }
        if(camera.position.y<=-10.40)
        {
            camera.position.y=-10.40;
        }
    }
    window.addEventListener('keydown', function() {
        clavier[event.keyCode] = true;
        }
    );
    window.addEventListener('keyup', function() {
        clavier[event.keyCode] = false;
        }
    );
    Init();
    //InitFog();
    Animer();
}